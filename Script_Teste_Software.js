var io = require('socket.io-client');
var io_server = require('socket.io').listen(3001);

describe('Testes de Socket - Fase 1 - Projeto Tetrinet ', function() {

  var socket;

  beforeEach(function(done) {
    // Configurações de Scokets
    socket = io.connect('http://localhost:3001', {
      'reconnection delay' : 0
      , 'reopen delay' : 0
      , 'force new connection' : true
      , transports: ['websocket']
    });

    //Conecta socket
    socket.on('connect', () => {
      done();
    });

    //Desconecta o socket
    socket.on('disconnect', () => {
      // console.log('Desconectando...');
    });
  });

  afterEach((done) => {
    // Limpar Tudo
    if(socket.connected) {
      socket.disconnect();
    }
    io_server.close();
    done();
  });

  it('Comunicação', (done) => {
    // Se connectado emite uma mensagem
    io_server.emit('echo', 'Socket Conectado!');

    socket.once('echo', (message) => {
      // Faz um teste na mensagem se está igual ao que foi mandado pelo socket
      expect(message).to.equal('Socket Conectado!'); //TDD equal message
      done();
    });

    io_server.on('connection', (socket) => {
      expect(socket).to.not.be.null; //Teste method TDD
    });
  });

});